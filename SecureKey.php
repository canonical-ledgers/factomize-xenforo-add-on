<?php

namespace CanonicalLedgers\Factomize;

class SecureKey {

    public static function updateLastCheckedIDChainHeight($height) {
        SQLHelpers::updateTable('xf_canonicalledgers_factomize_security', array("last_block_height_checked" => $height));
    }


    public static function getPrivateKeyInfo() {
        $info = array(
            "current" => array(
            "location" => "file://internal_data/CanonicalLedgers_Factomize/pkey.pem",
            "directory" => "internal_data/CanonicalLedgers_Factomize"
        ),
            "temp" => array(
            "location" => "file://internal_data/CanonicalLedgers_Factomize/tmp_pkey.pem",
            "directory" => "internal_data/CanonicalLedgers_Factomize"
        )
        );

        return $info;
    }

    public static function signFactomPost($public_data) {
        $pkeyInfo = self::getPrivateKeyInfo();
        $pkey = openssl_pkey_get_private($pkeyInfo['current']['location']);
        $signature = "";
        openssl_sign(json_encode($public_data), $signature, $pkey);
        openssl_free_key($pkey);

        if (!$signature)
            return false;

        return $signature;
    }

    public static function testPublicKey ($pubkey, $storedKey) {

        $pkeyInfo = self::getPrivateKeyInfo();
        $path = substr($pkeyInfo[$storedKey]['location'], 7);
        if (!file_exists($path))
            return false;

        $pkey = openssl_pkey_get_private($pkeyInfo[$storedKey]['location']);
        $current_pubkey = openssl_pkey_get_details($pkey)['key'];

        openssl_free_key($pkey);

        $pubkey = preg_replace('/[\s\t\n]+/', "", $pubkey);
        $current_pubkey = preg_replace('/[\s\t\n]+/', "", $current_pubkey);
        return $pubkey === $current_pubkey;
    }

    public static function clearCachedTempKey () {
        SQLHelpers::updateTable('xf_canonicalledgers_factomize_security', array("temp_public_key" => ""));
    }

    public static function updateCachedCurrentKey ($newKey) {
        SQLHelpers::updateTable('xf_canonicalledgers_factomize_security', array("current_public_key" => $newKey));
    }

    public static function switchToTempKey () {
        $pkeyInfo = self::getPrivateKeyInfo();

        $tmpLoc = $pkeyInfo['temp']['location'];
        $currLoc = $pkeyInfo['current']['location'];

        $tmpLoc = substr($tmpLoc, 7);
        $currLoc = substr($currLoc, 7);
        rename($tmpLoc, $currLoc);
        self::clearCachedTempKey();
    }

    public static function validatePublicKeyToIDChain ($chain_id) {
        $factom = Handler::getFactomAPI();
        $id_entries = $factom->getAllEntriesInChain($chain_id);
        if (!array_key_exists(0, $id_entries)) {
            if (array_key_exists("error", $id_entries)) {
                if (gettype($id_entries['error']) == "string")
                    return Cron\Error\Error::failedToConnectToFactom($id_entries['error'], "SecureKey.php::validatePublicKeyToIDChain::FactomAPI-getAllEntriesInChain");
                else if (gettype($id_entries['error']) == "array" && array_key_exists("code", $id_entries['error'])) {
                    // Invalid JSON RPC returned - chain does not exist
                    return Cron\Error\Error::invalidIdentityChain($id_entries, "SecureKey.php::validatePublicKeyToIDChain");
                }
            }
            else {
                // Unknown error occurred - this should never occur
                return Cron\Error\Error::failedToConnectToFactom($id_entries, "SecureKey.php::validatePublicKeyToIDChain::FactomAPI-getAllEntriesInChain");
            }
        }

        if (count($id_entries) < 2)
            return Cron\Error\Error::invalidIdentityChain("No valid signing key found on identity chain (not enough entries)", "SecureKey.php::validatePublicKeyToIDChain");

        // Most recent entry is index 0, so the first (authority) entry is last index
        $authority_entry = array_pop($id_entries);

        $authority_pubkey_pem = $authority_entry['content'];
        $authority_pubkey = openssl_get_publickey($authority_pubkey_pem);

        // Loop through each entry until a matching signing key is found
        foreach ($id_entries as $entry) {
            if (!array_key_exists("content", $entry) || !array_key_exists("extids", $entry) || count($entry['extids']) < 2) {
                continue;
            }

            $potential_pubkey = $entry['content'];
            $sig_algorithm = $entry['extids'][0];
            $signature = $entry['extids'][1];

            if ($sig_algorithm !== "RSA-SHA512")
                continue;

            // Only trust entries that have been signed by authority key
            $entry_has_valid_signature = openssl_verify($potential_pubkey, $signature, $authority_pubkey, $sig_algorithm);
            if ($entry_has_valid_signature) {
                $matches_current_key = self::testPublicKey($potential_pubkey, 'current');
                if ($matches_current_key) {
                    if (!Listener::getCachedCurrentKey()) {
                        // This should never occur
                        self::updateCachedCurrentKey($potential_pubkey);
                    }
                    return Cron\Error\Error::noError();
                }
                else {
                    $matches_temp_key = self::testPublicKey($potential_pubkey, 'temp');
                    if ($matches_temp_key) {
                        self::switchToTempKey();
                        self::updateCachedCurrentKey($potential_pubkey);
                        return Cron\Error\Error::noError();
                    }
                    else {
                        return Cron\Error\Error::publicKeyOutOfDate();
                    }
                }
            }
        }

        return Cron\Error\Error::invalidIdentityChain("No valid signing key found on the identity chain!");
    }

    public static function getIDChainInfo () {
        $db = \XF::db();
        $id_info = SQLHelpers::getTableColumn ('xf_canonicalledgers_factomize_security', "last_block_height_checked", "current_public_key");
        $id_info = array('last_block_height_checked' => $id_info);
        $options = \XF::options();
        $id_info['identity_chain'] = $options->identityChainAddress;
        if (empty($id_info)) {
            return Error\Error::invalidIdentityChain("The identity chain table is empty.");
        }
        return $id_info;
    }

    // OPTIMIZE: Create another validation state column and return this if last_block_checked == height
    public static function pullIdentityChain ($height) {
        $id_info = self::getIDChainInfo();
        if (array_key_exists("error_occurred", $id_info))
            return $id_info;

        $id_chain = $id_info['identity_chain'];
        $last_block_checked = $id_info['last_block_height_checked'];

        if ($last_block_checked == $height)
            return true;

        $result = self::validatePublicKeyToIDChain($id_chain);
        if (!$result['error_occurred']) {
            self::updateLastCheckedIDChainHeight($height);
        }
        return $result;
    }

    public static function generateNewKey () {
        $config = array(
            "digest_alg" => "sha512",
            "private_key_bits" => 4096,
            "private_key_type" => OPENSSL_KEYTYPE_RSA,
        );
        $openssl_resource = openssl_pkey_new($config);

        $pkey = "";
        openssl_pkey_export($openssl_resource, $pkey);

        $pkey_info = self::getPrivateKeyInfo();

        $tmp_loc = $pkey_info['temp']['location'];
        $tmp_loc = substr($tmp_loc, 7);

        if (!is_dir($pkey_info['temp']['directory'])) {
          mkdir('internal_data/CanonicalLedgers_Factomize');
        }
        touch($tmp_loc);
        chmod($tmp_loc, 0600);
        file_put_contents($tmp_loc, $pkey);

        $pubkey = openssl_pkey_get_details($openssl_resource)['key'];

        return $pubkey;
    }
}
