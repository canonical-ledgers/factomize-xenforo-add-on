# Factomize - Xenforo Add-on

## Summary
The Factomize Xenforo Add-on is an add-on to the popular PHP forum platform
Xenforo. The Factomize add-on adds an immutable audit trail to a forum using
the Factom blockchain. After installing the add-on, an admin can select which
forums and sub-forums shall be "factomized". Users will then be able to
independently verify that threads and posts have existed in their current form
since at least the time they were added to the Factom blockchain.

When a thread is created on a factomized forum, a new chain is also created on
the Factom blockchain. Every post and modification (including deletion) that
occurs under this thread shall be appended to this chain. When a user views a
thread or post on a factomized forum, they will be able to see an icon
indicating whether the post has been committed to the Factom blockchain that
links to the entry on a Factom blockchain explorer. For any secured post, a
user shall be able to independently recompute the hash of the post data that
matches the corresponding Factom entry.

In order to establish authenticity of entries on the Factom blockchain, the
forum will have its own private signing key that every entry will be signed
with. The currently accepted signing key shall be established by referencing an
externally managed Identity Chain on the Factom blockchain. The Identity Chain
ID that the forum uses will be publicly available and should not change once
initially established. A user will be able to use the published Identity Chain
ID to determine the signing key that was valid at the time of a given entry,
and then use that to verify the signature of an entry.

## External Dependencies
- Licensed installation of XenForo 2.0 on a server where you can set up a CRON
  job or `systemd-timer`
- Access to the [JSON RPC API](https://docs.factom.com/api?shell#factomd-api)
  of a synced instance of [`factomd`](https://github.com/FactomProject/factomd)
- Secure access to the [JSON RPC
  API](https://docs.factom.com/api?shell#factom-walletd-api) of an instance of
[`factom-walletd`](https://github.com/FactomProject/factomd) with a funded
Entry Credit address under your control
- [Recommended]  [Optional]  [CLI CRON Job Runner XenForo
  add-on](https://xenforo.com/community/resources/cli-job-runner.6478/) to
drive XenForo's CRON jobs more reliably on forums with infrequent posts or
edits.

## Installation
Download the most recent zipped release found
[here](https://bitbucket.org/canonical-ledgers/factomize-xenforo-add-on/downloads/?tab=downloads).
This will unzip into a folder called `upload`, which maps to the top level
directory of a XenForo installation. Copy everything inside of
`upload/src/addons/` to the corresponding directory of your XenForo
installation. One way to make this easier is to create a symlink called
`upload` that points to your XenForo root directory. Then just unzip into the
directory containing the `upload` symlink. Your XenForo installation should now
contain the directories `<xenforo
root>/src/addons/CanonicalLedgers/Factomize/`.

In the Xenforo Admin panel and click on Add-ons. The Factomize add-on should
now show up as an uninstalled add-on. Click install and wait for XenForo to
finish the installation steps.

#### CLI CRON Job Runner
XenForo runs any CRON jobs that are due to be run only when a user performs a
significant user interaction like a making a post or an edit. (Note that simply
loading the page does not trigger any CRON jobs to run.) This works just fine
for most of the infrequently run XenForo CRON jobs. However, the Factomize
XenForo Add on works best when it can run its CRON job every minute. On a forum
where a users are not consistently making posts or edits, the queue of posts to
be factomized will not be processed promptly and after a time the entries can
grow stale, requiring the factomizing process to repeat previous steps.

For this reason we recommend using this [CLI CRON Job Runner XenForo
add-on](https://xenforo.com/community/resources/cli-job-runner.6478/) which
replaces browser driven XenForo CRON jobs with a command line command that can
be added to a proper Unix CRON tab or a systemd timer. On modern servers that
use systemd we recommend using the unit files we have provided in this repo:
`xenforo-run-jobs.service` and `xenforo-run-jobs.timer`. Modify the `User`
field in the `xenforo-run-jobs.server` file so that it matches the user that
PHP runs as on your server, which is probably the same user as what the web
server runs as. For Apache on Arch Linux this is the user `http`. On Apache on
Ubuntu this is `www-data`.  Simply place these unit files in
`/etc/systemd/systemd/` and run the following:
```
systemctl daemon-reload
systemctl enable xenforo-run-jobs.timer
systemctl start xenforo-run-jobs.timer
```
This sets up a timer that will attempt to run any scheduled XenForo CRON jobs
once a minute.

If you prefer to use a proper Unix CRON tab entry, contrary to the CLI CRON Job
Runner add-on's documentation, make sure that you are running the command from
the XenForo root directory like this: `cd /path/to/xenforo/ && php cmd.php
xf:run-jobs`. See [this
post](https://xenforo.com/community/threads/cli-job-runner.148636/#post-1262407)
for more information.

## Set up the add-on
Setting up the add-on requires several steps to occur outside of the forum.
Make sure that you have an instance of `factomd` and `factom-walletd`
accessible from your forum's server.

#### Fund an Entry Credit address
You can use [`factom-cli`](https://github.com/FactomProject/factom-cli) with
`factom-walletd` to purchase Entry Credits with Factoids. You must have a
funded Factoid address first. The `factom-cli` interface is well documented so
it is not repeated here.

#### Set up the Identity Chain
In order to prove the authenticity of entries made by your forum, you must
establish a signing key with which to sign all entries. This add-on looks for a
special Identity Chain to confirm that its signing key matches the public key
established by the Identity Chain. A forum owner can publish the Identity Chain
ID so that users can always look up the current signing key in use and
independently validate entries.

Use [these instructions and bash
scripts](https://bitbucket.org/canonical-ledgers/identity-chain-tools/src/master/)
to set up an Identity Chain, ideally from a secure offline computer.

#### Configure the Add-on Settings
Finally we can configure the settings for the Factomize Add-on. In the Xenforo
Admin Control Panel, click on "Add-ons > Add-ons". Next, click the "gear" icon
to the left of the Factomize Add-on in the Add-ons list, and then click
"Options" from the drop down that appears.

Add the Identity Chain ID that you created using the Identity Chain Tools. Add
the funded public Entry Credit Address that is stored with your instance of
`factom-walletd`. Add the `factomd` and `factom-walletd` endpoints and set any
usernames and passwords if your setup requires that.

Finally click "Save".

You should now be ready to start Factomizing your forums.

## Set a forum to be factomized
Forums, or nodes as Xenforo refers to them, are by default not set to be
secured/factomized. In order to set a forum to be factomized, click on "Forums
> Nodes" from the Xenforo Admin Control Panel and then click on the name of the
> forum or node you want to set to be Factomized.

About halfway down the "Edit forum" page you will see a checkbox that says
"Factomize all new threads and posts under this forum". Click this checkbox to
factomize any new post going forward.

There is another indented checkbox below the first that says "Factomize all
existing threads and posts under this forum". If you check this box, any
existing threads and posts will be queued to be factomized.

Click "save". Now your forum is being factomized and you will see the factomize
status badge next to each thread and post.

If at a later time you unselect a forum for factomization, the badges will not
display for that forum, but any existing entries will remain in the Xenforo
database. If you later reselect the forum to be factomized, only posts and
threads that have been created or edited since the last time it was factomized
will be queued for refactomization.

## Make sure it works properly
After following the above steps, any new threads and posts made on a factomized
forum, as well as any edits or deletions, will be added to a queue for
processing onto the Factom blockchain. The queue is processed once a minute and
each item in the queue corresponds to a new entry on the Factom blockchain.
Each entry requires multiple steps that may take up to roughly 10-15 minutes in
total from the first step.

To test the functionality, create a new thread on a factomized forum. When
viewing the thread there should be a badge at the top of the thread and a badge
at the top of each post that says "Pending", "Unverified" or "Verified".
Clicking on this button will refresh the page while it says "pending".  After
the thread's chain has been created on the Factom blockchain, the button will
say "Verified" and will link to the chain on Factom explorer.

While waiting for a thread to be verified, users can still create and edit
posts. Each new post or edit will queue a new entry to be added to the thread's
chain. You can see this process happening by viewing the
`xf_canonicalledgers_factomize_queue` table. About once a minute, you should
see the values in the column `current_step` go from `commit` to `reveal` to
`dblockconfirm` for each row in the queue. Once an entry has been confirmed to
be recorded in the blockchain, it is removed from the queue and its chain id or
entry hash is added to the `xf_thread` or `xf_post` tables.

#### Error Logs
The Factomize add-on uses the Xenforo Server error log. Every time an error
event occurs while processing the queue, a log entry will be added here and the
CRON job will exit. If the `current_step` for rows in the queue aren't
advancing, check the error log.

Errors will generally only occur if:
- The Identity Chain is improperly configured.
- The `factomd` API is unreachable.
- The `factom-walletd` API is unreachable.
- The `factomd` API is unreachable by `factom-walletd`.

## Troubleshooting
If new posts and threads are not creating new rows in the queue make sure that
the forum you are creating threads and posts on is set to be factomized in the
`xf_forum` table.

If no errors have occurred but the `current_step` for rows in the queue isn't
changing, the CRON job may not be running. If you don't have the CLI CRON Job
Running add on installed, try creating a new post and see if the queue advances
one step. You can also manually trigger the queue processing CRON job to run
from the Tools > CRON Entries panel in the Admin CP.

If you are getting an error about an API call timeout, make sure that `factomd`
and `factom-walletd` are reachable from your server. Also make sure that you
have told `factom-walletd` the `factomd` API endpoint by using the `-s` flag
when starting `factom-walletd`.

If you are experiencing other errors make sure that your EC address is funded.
