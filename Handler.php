<?php

namespace CanonicalLedgers\Factomize;
use XF;

class Handler {
    public static function loadConfigs() {
        $conf_loc = "src/addons/CanonicalLedgers/Factomize/dev_config.txt";
        if (!file_exists($conf_loc))
            return false;
        $config = json_decode(file_get_contents($conf_loc), true);
        return $config;
    }

    public static function devLog($output, $sep="\n\n", $json=true)
    {
        $config = self::loadConfigs();
        if ($config === false)
            return;
        $ip = $config['ip'];
        $port = $config['port'];
        $oldErrorReporting = error_reporting();
        error_reporting($oldErrorReporting ^ E_WARNING);
        $stream = stream_socket_client($ip . ":" . $port, $errno, $errstr, 5, STREAM_CLIENT_CONNECT);
        error_reporting($oldErrorReporting);

        if (!$stream) {
            return;
        }

        if ($json)
            $output = json_encode($output);

        fwrite($stream, $output . $sep);

        stream_socket_shutdown($stream, STREAM_SHUT_WR);

        $contents = stream_get_contents($stream);
    }

    // Returns the factomize security table as an array
    public static function getSecurityTable () {
        $security = SQLHelpers::getTableRow("xf_canonicalledgers_factomize_security");
        return $security;
    }

    // Returns a FactomAPI object with information including endpoints, usernames and passwords already loaded
    public static function getFactomAPI () {
        $options = \XF::options();
        $factom = new FactomAPI($options->entryCreditAddress, $options->factomdEndpointAddress, $options->factomWalletdEndpointAddress);
        if ($options->factomdUsername != "" && $options->factomdPassword != "")
            $factom->setFactomdUserPass($options->factomdUsername, $options->factomdPassword);
        if ($options->factomWalletdUsername != "" && $options->factomWalletdPassword != "")
            $factom->setFactomWalletdUserPass($options->factomWalletdUsername, $options->factomWalletdPassword);

        return $factom;
    }

    // Gets the chain ID attached to a thread in xf_thread
    public static function getChainID($thread_id) {
        $where = "thread_id = " . $thread_id;
        $result = SQLHelpers::getTableColumn('xf_thread', 'canonicalledgers_factomize_chain_id', $where);
        if ($result === false)
            return "0";
        return $result;
    }

    // Gets the verification post status of a post in xf_post
    public static function getPostStatus ($post_id) {
        $where = "post_id = " . $post_id;
        $result = SQLHelpers::getTableColumn('xf_post', 'canonicalledgers_factomize_verification_status', $where);
        return $result;
    }

    // Sets the verification post status of a post in xf_post
    public static function setPostStatus ($post_id, $status) {
        $where = "post_id = " . $post_id;
        SQLHelpers::updateTable('xf_post', array('canonicalledgers_factomize_verification_status' => $status), $where);
    }
    // Sets the public data available with a post in xf_post
    public static function setPublicData ($post_id, $data) {
        $where = "post_id = " . $post_id;
        SQLHelpers::updateTable('xf_post', array('canonicalledgers_factomize_public_data' => $data), $where);
    }

    // Gets the public data associated with a post
    public static function getPublicData ($post_id) {
        $where = "post_id = " . $post_id;
        $result = SQLHelpers::getTableColumn('xf_post', 'canonicalledgers_factomize_public_data', $where);
        return $result;
    }

    // Gets the post entry hash of a post in xf_post
    public static function getPostEntryHash ($post_id) {
        $thread_finder = \XF::finder('XF::Post');
        return $thread_finder->where('post_id', $post_id)->fetchOne()->canonicalledgers_factomize_entry_hash;
    }

    // Sets the post entry hash of a post in xf_post
    public static function setPostEntryHash ($post_id, $status) {
        $where = "post_id = " . $post_id;
        SQLHelpers::updateTable('xf_post', array('canonicalledgers_factomize_entry_hash' => $status), $where);
    }

    // Inserts an entire new row into the factomize queue
    public static function addToFactomizeQueue ($extids, $data, $content_type, $current_step, $post_id, $thread_id, $chain_id="") {
        $colvals = array(
            "public_data" => $data,
            "extids" => $extids,
            "content_type" => $content_type,
            "current_step" => $current_step,
            "post_id" => $post_id,
            "thread_id" => $thread_id,
            "chain_id" => $chain_id,
            "last_step_performed_date" => time()
        );

        SQLHelpers::dbInsert("xf_canonicalledgers_factomize_queue", $colvals);
    }

    // Returns the factomize info table, which contains versioning information
    public static function getAddonInfo () {
        return SQLHelpers::getTableRow("xf_canonicalledgers_factomize_info");
    }

    // Gets the external ids for a thread/chain to be committed with
    public static function getThreadExtIds ($thread_id) {
        $id_chain_info = SecureKey::getIDChainInfo();

        if (array_key_exists('error_occurred', $id_chain_info))
          return $id_chain_info;

        $addon_info = self::getAddonInfo();

        $chain_extids = array();
        $chain_extids[] = "RSA-SHA512";
        $chain_extids[] = "IdentityChainID=" . $id_chain_info['identity_chain'];
        $chain_extids[] = "Factomize Xenforo add-on";
        $chain_extids[] = "Thread Chain";
        $chain_extids[] = self::getAddonInfo()['thread_chain_protocol'];
        $chain_extids[] = \XF::options()->boardUrl . "/index.php?threads/" . $thread_id;

        return $chain_extids;
    }

    // Gets and forms the public thread data for a thread/chain
    public static function getThreadChainData ($thread_id) {
        $thread_finder = \XF::finder('XF:Thread');
        $node_id = $thread_finder->where('thread_id', $thread_id)->fetchOne()->node_id;
        $post_date = $thread_finder->where('thread_id', $thread_id)->fetchOne()->post_date;

        $chain_data = array();
        $chain_data["description"] = "This chain records all activity for a thread.";
        $chain_data["thread_link"] = \XF::options()->boardUrl . "/index.php?threads/" . $thread_id;
        $chain_data["entry_date"] = time();
        $chain_data["thread_data"] = array(
          "post_date" => $post_date,
          "node_id" => $node_id,
          "thread_id" => $thread_id
        );

        return $chain_data;
    }

    // Adds a thread to the factomize queue based only on the thread_id
    public static function addThreadToQueue($thread_id) {
        $chain_extids = self::getThreadExtIds($thread_id);
        $chain_data = self::getThreadChainData($thread_id);

        self::addToFactomizeQueue($chain_extids, $chain_data, "thread", "commit", 0, $thread_id);
    }

    // Gets the public data for a post, including hashing the message
    // (and title if it is the first post) using sha512
    public static function getPublicPostData($post_id, $thread_id) {
        $post_finder = \XF::finder('XF:Post');
        $thread_finder = \XF::finder('XF:Thread');
        $first_post_id = $thread_finder->where('thread_id', $thread_id)->fetchOne()->first_post_id;
        $user_id = $post_finder->where('post_id', $post_id)->fetchOne()->user_id;
        $last_edit_user_id = $post_finder->where('post_id', $post_id)->fetchOne()->last_edit_user_id;
        $post_date = $post_finder->where('post_id', $post_id)->fetchOne()->post_date;
        $last_edit_date = $post_finder->where('post_id', $post_id)->fetchOne()->last_edit_date;
        $title = $thread_finder->where('thread_id', $thread_id)->fetchOne()->title;
        $node_id = $thread_finder->where('thread_id', $thread_id)->fetchOne()->node_id;
        $message = $post_finder->where('post_id', $post_id)->fetchOne()->message;
        $edit_count = $post_finder->where('post_id', $post_id)->fetchOne()->edit_count;

        $public_data = array();
        $public_data['post_link'] = \XF::options()->boardUrl . "/index.php?threads/" . $thread_id . "#post-" . $post_id;

        $post_data = array();
        $post_data['post_date'] = $post_date;
        $post_data['node_id'] = $node_id;
        $post_data['thread_id'] = $thread_id;
        $post_data['user_id'] = $user_id;
        $post_data['last_edit_user_id'] = $last_edit_user_id;
        $post_data['last_edit_date'] = $last_edit_date;
        $post_data['edit_count'] = $edit_count;
        if ($post_id == $first_post_id)
          $post_data['title_sha512'] = hash('sha512', $title);
        $post_data['message_sha512'] = hash('sha512', $message);

        $public_data['post_data'] = $post_data;

        return $public_data;
    }

    // Gets the external post ids in an array
    public static function getPostExtIds ($thread_id, $post_id=0) {
      $id_chain_info = SecureKey::getIDChainInfo();

      $extids = array();
      $extids[] = "RSA-SHA512";
      $extids[] = "IdentityChainID=" . $id_chain_info['identity_chain'];
      $extids[] = "Forum Post";
      $extids[] = self::getAddonInfo()['thread_chain_protocol'];
      $extids[] = \XF::options()->boardUrl . "/index.php?threads/" . $thread_id . "#post-" . $post_id;

      return $extids;
    }

    // checks if a post is the first post in a chain simply by comparing it to the first_post_id column in xf_thread
    public static function checkIfIsFirstPost ($post_id, $thread_id) {
        $thread_finder = \XF::finder('XF:Thread');
        $first_post_id = $thread_finder->where('thread_id', $thread_id)->fetchOne()->first_post_id;
        return $post_id == $first_post_id;
    }

    // adds a post to the queue based on post_id and thread_id
    public static function addPostToQueue ($post_id, $thread_id) {
        $public_data = self::getPublicPostData($post_id, $thread_id);

        $previous_public_data = self::getPublicData($post_id);
        if ($previous_public_data)
            $previous_public_data = json_decode($previous_public_data, 1);

        // Nothing has changed, this post would be a repeat
        if ($public_data['post_data']['message_sha512'] === $previous_public_data['post_data']['message_sha512']
            && $public_data['post_data']['edit_count'] === $previous_public_data['post_data']['edit_count']) {
                return;
        }

        $extids = self::getPostExtIds($thread_id, $post_id);

        $chain_id = self::getChainID($thread_id);

        self::addToFactomizeQueue($extids, $public_data, "post", "commit", $post_id, $thread_id, $chain_id);
        self::setPostStatus($post_id, "Pending");
    }

    // sets a node to not be factom secured going forward
    public static function unsetFactomizeNode ($node_id) {
        $where = "node_id = " . $node_id;
        SQLHelpers::updateTable('xf_forum', array('canonicalledgers_factomize_secure' => 0), $where);
    }

    // sets a node to be factom secured going forward
    public static function setFactomizeNode ($node_id) {
        $where = "node_id = " . $node_id;
        SQLHelpers::updateTable('xf_forum', array('canonicalledgers_factomize_secure' => 1), $where);
    }

    // checks whether a forum node is factom secured
    public static function checkFactomizeNode ($node_id) {
        $where = "node_id = " . $node_id;
        $result = SQLHelpers::getTableColumn('xf_forum', 'canonicalledgers_factomize_secure', $where);
        return $result;
    }

    // checks whether a thread is part of a factom secured node factom secured
    public static function checkFactomizeThread ($thread_id) {
        $thread_finder = \XF::finder('XF:Thread');
        $node_id = $thread_finder->where('thread_id', $thread_id)->fetchOne()->node_id;

        return self::checkFactomizeNode($node_id);
    }

    // checks whether a post is part of a factom secured node factom secured
    public static function checkFactomizePost ($post_id) {
        $post_finder = \XF::finder('XF::Post');
        $thread_id = $post_finder->where('post_id', $post_id)->fetchOne()->thread_id;

        return self::checkFactomizeThread($thread_id);
    }

    // Removes the entry hash from a post so that the verified status resets
    public static function clearPostEntryHash ($post_id) {
        self::setPostEntryHash($post_id, "");
    }

    // Adds an entry to the queue and creates a new chain for it if specified
    public static function queueEntry ($post_id=0, $thread_id=0, $createThread=false) {
        if ($createThread) {
            self::addThreadToQueue($thread_id);
        }
        self::addPostToQueue($post_id, $thread_id);
    }

    // BEGIN DELETE HANDLER FUNCTIONS
    public static function getDeleteData ($thread_id, $post_id=0) {
        $data = array();
        $data['message'] = "This entry records the deletion of a " . ($post_id == 0 ? "thread." : "post.");
        $data['thread_id'] = $thread_id;
        if ($post_id != 0)
            $data['post_id'] = $post_id;

        return $data;
    }

    public static function addDeleteToQueue ($thread_id, $post_id=0) {
        $public_data = self::getDeleteData($thread_id, $post_id);
        $extids = self::getPostExtIds($thread_id, $post_id);

        // type is always treated as post in the case of a delete so that it knows it falls under a thread chain
        $type = "post";

        self::addToFactomizeQueue($extids, $public_data, $type, "commit", $post_id, $thread_id);
    }

    // BEGIN FACTOMIZE PREV FUNCTIONS
    public static function getAllPostsInThread ($thread_id) {
        $where = "thread_id = " . $thread_id;
        return SQLHelpers::getTableColumns("xf_post", "post_id", $where);
    }

    public static function addEntireThreadToQueue ($thread_id) {
        self::addThreadToQueue($thread_id);

        $posts = self::getAllPostsInThread($thread_id);
        foreach ($posts as $post) {
            self::addPostToQueue($post['post_id'], $thread_id);
        }
    }

    public static function addExistingNodeToQueue ($node_id) {
        // Newly created forums aren't assigned node id's until after this function is called, so we can exit immediately if this is the case
        // as newly created forums won't have any existing posts or threads
        if (!$node_id)
            return false;

        $where = "node_id = " . $node_id;
        $threads_in_node = SQLHelpers::getTableColumns("xf_thread", array("thread_id", "canonicalledgers_factomize_chain_id"), $where);
        if ($threads_in_node === false || count($threads_in_node) <= 0)
            return false;
        else if (gettype($threads_in_node) != "array" || (gettype($threads_in_node) == "array" && !array_key_exists("0", $threads_in_node)))
            $threads_in_node = array($threads_in_node);


        foreach ($threads_in_node as $node_thread) {
            if ($node_thread['canonicalledgers_factomize_chain_id'] == "0") {
                $thread_in_queue = self::checkIfThreadInQueue($node_thread['thread_id']);
                if (!$thread_in_queue) {
                    // If there is no chain_id on xf_thread and the thread is not being created in the queue,
                    // then it's okay to create and add the entire thread to queue (if there are posts with the thread id in the queue,
                    // but the thread isn't being created, then these will never be processed and it's okay to do everything over again)
                    self::addEntireThreadToQueue($node_thread['thread_id']);
                    continue;
                }
            }
            else {
                $where = "thread_id = " . $node_thread['thread_id'] . " AND canonicalledgers_factomize_entry_hash = 0";
                $unconfirmed_posts = SQLHelpers::getTableColumns("xf_post", "post_id", $where);
                foreach ($unconfirmed_posts as $unconfirmed_post) {
                    $post_finder = \XF::finder('XF:Post');
                    $current_post_edit_count = $post_finder->where('post_id', $unconfirmed_post['post_id'])->fetchOne()->edit_count;

                    $post_in_queue = self::getPostInQueue($unconfirmed_post['post_id']);
                    if ($post_in_queue !== false) {
                        $queued_post_public_data = json_decode($post_in_queue['public_data'], 1);

                        $queued_post_edit_count = $queued_post_public_data['post_data']['edit_count'];

                        if ($current_post_edit_count !== $queued_post_edit_count)
                            self::addPostToQueue($unconfirmed_post['post_id'], $node_thread['thread_id']);
                    }
                    else
                        self::addPostToQueue($unconfirmed_post['post_id'], $node_thread['thread_id']);
                }
            }
        }

        $where = "canonicalledgers_factomize_entry_hash = 0";
        $unconfirmed = SQLHelpers::getTableColumns("xf_post", "post_id", $where);
        if ($unconfirmed === false)
            return false;
        else if (gettype($unconfirmed) != "array")
            $unconfirmed = array($unconfirmed);

    }

    public static function getPostInQueue ($post_id) {
        $where = "post_id = " . $post_id . " AND content_type = 'post'";
        $post_match = SQLHelpers::getTableRow("xf_canonicalledgers_factomize_queue", $where);
        if (gettype($post_match) !== "array")
            return false;
        return $post_match;
    }

    public static function checkIfThreadInQueue ($thread_id) {
        $where = "thread_id = " . $thread_id . " AND content_type = 'thread'";
        $thread_match = SQLHelpers::getTableRow("xf_canonicalledgers_factomize_queue", $where);

        return (gettype($thread_match) === "array");
    }
}
