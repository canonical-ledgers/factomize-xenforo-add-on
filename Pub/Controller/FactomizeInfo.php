<?php

namespace CanonicalLedgers\Factomize\Pub\Controller;

use XF\Mvc\ParameterBag;

class FactomizeInfo extends \XF\Pub\Controller\AbstractController
{
    public function actionIndex(ParameterBag $params)
    {
        $viewParams = [];
        return $this->view('CanonicalLedgers\Factomize:FactomizeInfo\View', 'canonicalledgers_factomize_info_view', $viewParams);
    }
}
