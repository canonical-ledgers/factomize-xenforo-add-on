<?php

// ################## THIS IS A GENERATED FILE ##################
// DO NOT EDIT DIRECTLY. EDIT THE CLASS EXTENSIONS IN THE CONTROL PANEL.

namespace CanonicalLedgers\Factomize\XF\Admin\Controller
{
	class XFCP_Forum extends \XF\Admin\Controller\Forum {}
}

namespace CanonicalLedgers\Factomize\XF\Pub\Controller
{
	class XFCP_Forum extends \XF\Pub\Controller\Forum {}
	class XFCP_Thread extends \XF\Pub\Controller\Thread {}
}

namespace CanonicalLedgers\Factomize\XF\Service\Thread
{
	class XFCP_Creator extends \XF\Service\Thread\Creator {}
	class XFCP_Deleter extends \XF\Service\Thread\Deleter {}
	class XFCP_Replier extends \XF\Service\Thread\Replier {}
}