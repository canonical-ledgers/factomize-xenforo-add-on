<?php
namespace CanonicalLedgers\Factomize;

use XF;

class SQLHelpers {
    // Encapsulates a passed string in quotes
    public static function quotify ($str) {
        return "'" . $str . "'";
    }

    // Takes an array and turns it into a comma delimited list with optional quotes
    // around each item
    public static function listify ($val_array, $quotes=true) {
        if (gettype($val_array) != "array")
            return false;

        if (gettype($val_array[0]) == "array")
            $val_array[0] = json_encode($val_array[0]);

        if ($quotes)
            $list = self::quotify($val_array[0]);
        else
            $list = $val_array[0];

        foreach (array_slice($val_array, 1) as $val) {
            if (gettype($val) == "array")
                $val = json_encode($val);
            $list .= ", ";
            if ($quotes)
                $list .= self::quotify($val);
            else
                $list .= $val;
        }

        return $list;
    }

    // Turns a passed array into a list and then encapsulates this in parenthesis,
    // with optional quotes for each item
    public static function parenify ($val_array, $quotes=true) {
        return "(" . self::listify($val_array, $quotes) . ")";
    }

    // Uses sql to insert a new row into a specified table with a key-value pair array where the key
    // is a column and the value is the desired column value
    public static function dbInsert ($table, $keyvals) {
        if (gettype($keyvals) != "array")
            return false;

        $cols = array_keys($keyvals);
        $vals = array_values($keyvals);

        $stmt = "INSERT INTO " . $table . " ";
        $stmt .= self::parenify($cols, false) . " ";
        $stmt .= "VALUES" . self::parenify($vals);

        \XF::db()->query($stmt);
    }

    // Returns a SQL statement string for select columns from a table with optional where and limit clauses
    private static function getSelectTableStatement ($table, $cols="*", $where="", $limit=-1) {
        if (gettype($table) != "string")
            return false;

        if (gettype($cols) == "array")
            $cols = self::listify($cols, false);

        $stmt = "SELECT " . $cols . " ";
        $stmt .= "FROM " . $table . " ";

        if (gettype($where) == "string" && $where !== "") {
            $stmt .= "WHERE " . $where . " ";
        }

        if (gettype($limit) == "integer" && $limit > 0)
            $stmt .= "LIMIT " . $limit;

        return $stmt;
    }

    // Returns a SQL statement string for updating specified columns in a table with an optional where clause
    public static function getUpdateTableStatement ($table, $cols, $where="") {
        if (gettype($table) != "string" || gettype($cols) != "array")
            return false;

        $stmt = "UPDATE " . $table . " ";
        $stmt .= "SET ";
        foreach ($cols as $col => $value) {
            $stmt .= $col . " = '" . $value . "', ";
        }
        $stmt = substr($stmt, 0, strlen($stmt) - 2) . " ";

        if ($where != "")
            $stmt .= "WHERE " .  $where;

        return $stmt;
    }

    // Executes the update table statement using the Xenforo database adapter
    public static function updateTable ($table, $cols, $where="") {
        $stmt = self::getUpdateTableStatement($table, $cols, $where);
        \XF::db()->query($stmt);
    }

    // Executes the select table statement for a single row using the Xenforo database adapter
    public static function getTableRow ($table, $where="") {
        $stmt = self::getSelectTableStatement($table, "*", $where, 1);
        return \XF::db()->fetchRow($stmt);
    }

    // Takes a table name and returns every row and column in it
    public static function getTable ($table) {
        $stmt = self::getSelectTableStatement($table);
        return \XF::db()->fetchAll($stmt);
    }

    // Gets a single or all columns from a table, passed as a string, and has optional where and limit clauses
    public static function getTableColumn ($table, $cols="*", $where="", $limit=-1) {
        $stmt = self::getSelectTableStatement($table, $cols, $where, $limit);
        $result =  \XF::db()->fetchAllColumn($stmt);
        if (count($result) == 1)
            return $result[0];
        else if (count($result) > 1)
            return $result;
        else
            return false;
    }

    // Gets multiple columns from a table with optional where and limit clauses
    public static function getTableColumns ($table, $cols="*", $where="", $limit=-1) {
        $stmt = self::getSelectTableStatement($table, $cols, $where, $limit);
        return \XF::db()->fetchAll($stmt);
    }

    // Forms a SQL delete statement with a where clause in a specified table
    public static function getDeleteTableRowStatement ($table, $where) {
        if (gettype($table) != 'string' || gettype($where) != 'string')
            return;
        $stmt = "DELETE FROM " . $table;
        $stmt .= " WHERE " . $where;
        return $stmt;
    }

    // Deletes row(s) from a specified table based on the where clause
    public static function deleteTableRow ($table, $where) {
        $stmt = self::getDeleteTableRowStatement($table, $where);
        \XF::db()->query($stmt);
    }
}
