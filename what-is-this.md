# What is the Factomize Xenforo Add-on?
The Factomize Xenforo Add-on creates immutable receipts of forum activity on
the Factom blockchain. This provides proof that a forum post has existed in its
current form since at least the time the corresponding entry was made on the
Facotm blockchain.

## Who built it?
Two Authority Node Operators, Factomize and Canonical Ledgers, partnered to
create this forum plugin to add additional security to the defacto official
Factom Community Forum and showcase the Factom blockchain for establishing data
integrity.

## How do I use it?
You can continue to use the forum as you normally would.

## How does it work?
When you create a thread on a Factomized forum, a new chain will be create on
the Factom blockchain. Whenever a post is made on a Factomized thread, a new
entry will be appended to the thread's chain containing a hash of the posts
content as well as some other metadata. While an entry is being processed, the
post will have a badge that says "Pending". Once the entry has been created,
the post's badge will change to "Secured".

## How do I verify a post against its entry on the Factom blockchain?
Currently verifying a post is a somewhat technical process that requires
reading raw data from the Factom blockchain using the `factomd` API. The below
assumes you know how to do this using Curl or the `factom-cli` utility.

Clicking on the "Secured" badge above a post will expand a section below that
post displaying the necessary data to verify an entry against the blockchain.
This shall be refered to as the "security section" of a post. There are a few
things to verify about an entry:

1. The entry has a valid signature by the Signing Key established at the time
   that the entry was created. The public Signing Key can be found on the
forum's Identity Chain, which is linked to at the top of every thread. An entry
made by this add-on will have the type of signature in its 1st External ID, and
the the raw signature data in its 2nd External ID. Currently we use OpenSSL to
generate RSA-SHA512 signatures.
2. The entry's content is exactly equal to the minified JSON displayed in the
   text box labeled "Entry Content" in the post's security section.
3. The value of the `message_sha512` field of the JSON in the entry's content
   matches the SHA512 hash of the data found in the text box labeled "Raw
Message Content" in the post's security section.
4. If present, the value of the `title_sha512` field of the JSON in the entry's
   content matches the SHA512 hash of the data found in the text box labeled
"Raw Title Content" in the post's security section.
