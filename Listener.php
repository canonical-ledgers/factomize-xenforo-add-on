<?php
namespace CanonicalLedgers\Factomize;

use XF\Mvc\Entity\Entity;

use XF;
use XF\DevelopmentOutput;
use XF\Db\Schema\Alter;

// Functions in this class are used as entry points from Xenforo
class Listener
{
    // Extends the Xenforo entity structure to include the canonicalledgers_factomize_secure column/property
    public static function forumEntityStructure (\XF\Mvc\Entity\Manager $em, \XF\Mvc\Entity\Structure &$structure) {
      $structure->columns['canonicalledgers_factomize_secure']=['type' => Entity::BOOL, 'default' => false];
      $structure->columns['canonicalledgers_factomize_existing']=['type' => Entity::BOOL, 'default' => false];
    }

    // Extends the Xenforo entity structure to include the canonicalledgers_factomize_chain_id column/property
    public static function threadEntityStructure (\XF\Mvc\Entity\Manager $em, \XF\Mvc\Entity\Structure &$structure) {
      $structure->columns['canonicalledgers_factomize_chain_id']=['type' => Entity::STR, 'default' => "0"];
    }

    // Extends the Xenforo entity structure to include the canonicalledgers_factomize_verification_status and entry_hash columns/properties
    public static function postEntityStructure (\XF\Mvc\Entity\Manager $em, \XF\Mvc\Entity\Structure &$structure) {
      $structure->columns['canonicalledgers_factomize_verification_status']=['type' => Entity::STR, 'default' => "Unz"];
      $structure->columns['canonicalledgers_factomize_entry_hash']=['type' => Entity::STR, 'default' => "0"];
      $structure->columns['canonicalledgers_factomize_public_data']=['type' => Entity::STR];
      $structure->columns['canonicalledgers_factomize_verification_status']=['type' => Entity::STR, 'default' => "Unverified"];
    }

    // Uses the validation callback of the "generate new signing key" check box in add-on options to trigger
    // the generation of a new key. Always returns false so that the checked value is reset.
    public static function getGenerateKeyPress ($generateSelected)
    {
        if ($generateSelected === "generate") {
            $pubkey = SecureKey::generateNewKey();
            SQLHelpers::updateTable('xf_canonicalledgers_factomize_security', array("temp_public_key" => $pubkey));
        }
        return false;
    }

    // Returns the temporary public key stored in the security table
    public static function getCachedTempKey () {
        $pubkey = SQLHelpers::getTableColumn('xf_canonicalledgers_factomize_security', 'temp_public_key');
        return $pubkey;
    }

    public static function getCachedCurrentKey () {
        $pubkey = SQLHelpers::getTableColumn('xf_canonicalledgers_factomize_security', array("current_public_key"));
        return $pubkey;
    }

    // Checks to make sure the entry credit field is right length (52 characters),
    // only contains letters and numbers and starts with "ec" or "EC"
    public static function verifyEntryCreditAddress ($user_input) {
        $verified = preg_match('/^(ec|EC)[0-9a-zA-Z]{50}$/', $user_input);
        return $verified === 1;
    }

    // Checks to make sure only letters and numbers are used and it is 64 characters
    // long
    public static function verifyIdentityChainAddress ($user_input) {
        $verified = preg_match('/^[0-9a-zA-Z]{64}$/', $user_input) === 1;

        return $verified;
    }

    // Matches either IP addresses or domain names, restricting everything to be url safe characters
    public static function verifyEndpointAddress ($user_input) {
        $verified = preg_match("/^[a-zA-Z\w\-\._~:\/\?#[\]@!\$&'\(\)\*\+,;=.]+$/", $user_input);
        return $verified === 1;
    }

    public static function verifyFactomWalletdPassword ($password) {
        return true;
        return ($password !== "");
    }

    public static function verifyFactomdPassword ($password) {
        return ($password !== "");
    }

    public static function getEntryCreditAddressBalance ($ec_address) {
        $factom = Handler::getFactomAPI();
        $balance = $factom->getEntryCreditBalance($ec_address);
        if (gettype($balance) == "array" && array_key_exists("result", $balance))
            return $balance['result']['balance'];
        else
            return "N/A";
    }

    public static function verifyAddressInWalletd ($ec_address) {
        $factom = Handler::getFactomAPI();
        $address = $factom->getAddress($ec_address);
        if (gettype($address) == "array" && array_key_exists("result", $address))
            return "true";
        else
            return "false";
    }

    // Uses the URL template in the database to construct an explorer link by using a key-value
    // array and replacing all keys found in the string with the corresponding value
    public static function getExplorerLinkUrlFromTemplate ($params) {
        if (gettype($params) == "string") {
            $params = json_decode($params, 1);
        }
        $url = SQLHelpers::getTableColumn('xf_canonicalledgers_factomize_security', 'explorer_template_link');
        foreach ($params as $find => $replace) {
            $url = str_replace($find, $replace, $url);
        }
        return $url;
    }

    // A code event listener that is called on post edit/save/soft delete and adds to queue if secured
    public static function postEntityPostSave(\XF\Mvc\Entity\Entity $entity)
    {
        if ($entity->isUpdate()) {
            $post_id = $entity->post_id;
            $thread_id = $entity->thread_id;
            $thread_finder = \XF::finder('XF::Thread');
            Handler::clearPostEntryHash($post_id);

            $secure = Handler::checkFactomizeThread($thread_id);
            if ($secure) {
                if ($entity->message_state === "deleted")
                    Handler::addDeleteToQueue($entity->thread_id, $entity->post_id);
                else
                    Handler::queueEntry($post_id, $thread_id);
            }
        }
    }

    // A code event listener that gets called when a thread is deleted and adds to queue if secured
    public static function threadEntityPostSave(\XF\Mvc\Entity\Entity $entity)
    {
        $secure = Handler::checkFactomizeThread($entity->thread_id);
        if ($secure && $entity->discussion_state === "deleted") {
            Handler::addDeleteToQueue($entity->thread_id);
        }
    }

    // A code event listener that gets called when a post is hard deleted and adds to queue if secured
    public static function postEntityPostDelete(\XF\Mvc\Entity\Entity $entity)
    {
        $secure = Handler::checkFactomizeThread($entity->thread_id);
        if ($secure)
              Handler::addDeleteToQueue($entity->thread_id, $entity->post_id);
    }
}
