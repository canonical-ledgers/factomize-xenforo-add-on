<?php

namespace CanonicalLedgers\Factomize\XF\Service\Thread;
use CanonicalLedgers\Factomize\Handler;

class Deleter extends XFCP_Deleter
{
    protected $secured;

    public function setSecure($secured)
    {
        $this->secured = $secured;
    }

    public function delete ($type, $reason='')
    {
        $result = parent::delete($type, $reason);
        if ($this->thread->Forum->canonicalledgers_factomize_secure) {
            Handler::addDeleteToQueue($this->thread->thread_id);
        }

        return $result;
    }
}
