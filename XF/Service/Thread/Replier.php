<?php

namespace CanonicalLedgers\Factomize\XF\Service\Thread;
use CanonicalLedgers\Factomize\Handler;

// Extends the thread's replier, which is called when new posts are created
class Replier extends XFCP_Replier
{
    protected $secured;

    public function setSecure($secured)
    {
        $this->secured = $secured;
    }

    protected function _save ()
    {
        $post = parent::_save();
        if ($this->secured) {
            Handler::queueEntry($post->post_id, $post->thread_id);
        }


        return $post;
    }
}
