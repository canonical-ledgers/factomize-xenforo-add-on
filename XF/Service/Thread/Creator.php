<?php

namespace CanonicalLedgers\Factomize\XF\Service\Thread;

use CanonicalLedgers\Factomize\Handler;

/** @var \CanonicalLedgers\Factomize\Entity\secured $secured */

// Extend's forum's creator, which is called when new threads are created
class Creator extends XFCP_Creator
{
    protected $secured;

    public function setSecure($secured)
    {
        $this->secured = $secured;
    }

    protected function _save()
    {
        $thread = parent::_save();

        if ($this->secured) {
            $createThread = true;
            Handler::queueEntry($thread->first_post_id, $thread->thread_id, $createThread);
        }

        return $thread;
    }
}
