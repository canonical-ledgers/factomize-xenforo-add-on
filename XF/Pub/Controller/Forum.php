<?php

namespace CanonicalLedgers\Factomize\XF\Pub\Controller;

class Forum extends XFCP_Forum
{
    protected function setupThreadCreate(\XF\Entity\Forum $forum)
    {
        /** @var \CanonicalLedgers\Factomize\XF\Service\Thread\Creator $creator */
        $creator = parent::setupThreadCreate($forum);

        if ($forum->canonicalledgers_factomize_secure) {
            $creator->setSecure(true);
        }

        return $creator;
    }
}
