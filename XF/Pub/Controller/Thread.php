<?php

namespace CanonicalLedgers\Factomize\XF\Pub\Controller;
use CanonicalLedgers\Factomize\Handler;


class Thread extends XFCP_Thread
{
    protected function setupThreadReply(\XF\Entity\Thread $thread)
    {
        /** @var \CanonicalLedgers\Factomize\XF\Service\Thread\Replier $reply */
        $reply = parent::setupThreadReply($thread);

        if ($thread->Forum->canonicalledgers_factomize_secure) {
            $reply->setSecure(true);
        }

        $reply->canonicalledgers_factomize_chain_id =
            $this->filter('canonicalledgers_factomize_chain_id', 'string');
        return $reply;
    }
}
