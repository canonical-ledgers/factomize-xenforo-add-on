<?php

namespace CanonicalLedgers\Factomize\XF\Admin\Controller;

use XF\Mvc\FormAction;

use \CanonicalLedgers\Factomize\Handler;

class Forum extends XFCP_Forum
{
    protected function saveTypeData(FormAction $form, \XF\Entity\Node $node,
        \XF\Entity\AbstractNode $data)
    {
        parent::saveTypeData($form, $node, $data);

        $form->setup(function() use ($data)
        {
            $data->canonicalledgers_factomize_secure =
                $this->filter('canonicalledgers_factomize_secure', 'bool');
            $data->canonicalledgers_factomize_existing =
                $this->filter('canonicalledgers_factomize_existing', 'bool');

            if ($data->canonicalledgers_factomize_existing) {
                Handler::addExistingNodeToQueue($data->node_id);
            }
            if (!$data->canonicalledgers_factomize_secure) {
                $data->canonicalledgers_factomize_existing = false;
            }
        });
    }
}
