<?php

namespace CanonicalLedgers\Factomize;

class FactomAPI
{
    private $ec_address = "";
    private $factomd_url = "";
    private $factomd_username = "";
    private $factomd_password = "";
    private $factom_walletd_url = "";
    private $factom_walletd_username = "";
    private $factomd_walletd_password = "";
    private $prefer_ascii = true;
    public const ZEROHASH = "0000000000000000000000000000000000000000000000000000000000000000";
    public const FACTOMDVERSION = "v2";
    public const FACTOMWALLETDVERSION = "v2";

    public function __construct($ec, $factomd, $fwallet, $prfascii=true)
    {
        $this->ec_address = $ec;
        $this->factomd_url = $factomd;
        $this->factom_walletd_url = $fwallet;
        $this->prefer_ascii = $prfascii;

        $factomd_version_string =  "/" . self::FACTOMDVERSION;
        if (substr($this->factomd_url, -3) != $factomd_version_string)
            $this->factomd_url .= $factomd_version_string;

        $factom_walletd_version_string =  "/" . self::FACTOMWALLETDVERSION;
        if (substr($this->factom_walletd_url, -3) != $factom_walletd_version_string)
            $this->factom_walletd_url .= $factom_walletd_version_string;
    }

    public function setFactomdUserPass($user, $pass)
    {
        $this->factomd_username = $user;
        $this->factomd_password = $pass;
    }

    public function setFactomWalletdUserPass($user, $pass)
    {
        $this->factom_walletd_username = $user;
        $this->factom_walletd_password = $pass;
    }

    // Function that highly abstracts CURL post requests sent to the factomd and
    // the factomd-wallet API.
    public function direct_call($url, $methodName, $params)
    {
        $ch = curl_init();
        if (!empty($params)) {
            $postData = array(
                "jsonrpc" => "2.0",
                "id" => 0,
                "method" => $methodName,
                "params" => $params
            );
        }
        else {
            $postData = array(
                "jsonrpc" => "2.0",
                "id" => 0,
                "method" => $methodName
            );
        }

        $options = array(
            CURLOPT_HTTPHEADER => array("Content-Type: text/plain"),
            CURLOPT_POSTFIELDS => json_encode($postData),
            CURLOPT_URL => $url,
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 5
        );

        if ($url == $this->factomd_url && $this->factomd_username != "" && $this->factomd_password != "")
            $options[CURLOPT_USERPWD] = $this->factomd_username . ":" . $this->factomd_password;
        else if ($url == $this->factom_walletd_url && $this->factom_walletd_username != "" && $this->factom_walletd_password != "") {
            $options[CURLOPT_USERPWD] = $this->factom_walletd_username . ":" . $this->factom_walletd_password;
        }

        curl_setopt_array($ch, $options);

        $result = curl_exec($ch);
        $error = curl_error($ch);

        $json_result = json_decode($result, true);

        $result = $json_result == null ? array("error" => $result) : $json_result;
        $error = array("error" => $error);

        return $error['error'] == '' ? $result : $error;
    }

    // Helper function to convert string to hex
    public static function hexify($dataStr)
    {
        $hex = unpack('H*', $dataStr);
        return array_shift($hex);
    }

    // Helper function to convert hex to string
    public static function dehexify($hexStr)
    {
        $str = pack('H*', $hexStr);
        return $str;
    }

    // Helper function that takes a string, string array, or hexed string array
    // and returns a hexed string array
    private function getHexedArray($extIds) {
        $hexedIds = array();

        if (gettype($extIds) == "string") {
            $hexedIds[] = self::hexify($extIds);
        }

        else if (gettype($extIds) == "array") {
            foreach ($extIds as &$extId) {
                $hexedIds[] = self::hexify($extId);
            }
        }

        else {
            $hexedIds = $extIds;
        }

        return $hexedIds;
    }

    // Function that takes external ids (tags), and message content and returns
    // the response for the compose-chain method from the Factomd API. To
    // prevent collisions, this function also adds a unique identifier as an extId.
    public function composeChain($extIds, $content)
    {
        if (gettype($content) == "array") {
            $content = json_encode($content);
        }

        $hexedIds = $this->getHexedArray($extIds);
        // $hexedIds[] = self::hexify(uniqid());

        return $this->direct_call(
            $this->factom_walletd_url,
            "compose-chain",
            array(
                "chain" => array(
                    "firstentry" => array(
                        "extids" => $hexedIds,
                        "content" => self::hexify($content)
                    )
                ),
                "ecpub" => $this->ec_address
            )
        );
    }

    // Function that takes external ids (tags), and message content and returns
    // the response for the compose-entry method from the Factomd API. To
    // prevent collisions, this function also adds a unique identifier as an extId.
    public function composeEntry($chainId, $extIds, $content)
    {
        if (gettype($content) == "array") {
            $content = json_encode($content);
        }

        $hexedIds = $this->getHexedArray($extIds);

        return $this->direct_call(
            $this->factom_walletd_url,
            "compose-entry",
            array(
                "entry" => array(
                    "chainid" => $chainId,
                    "extids" => $hexedIds,
                    "content" => self::hexify($content)
                ),
                "ecpub" => $this->ec_address
            )
        );
    }


    // Wrapper function around a call to the commit-entry method in the Factomd API
    public function commitEntry($message)
    {
        return $this->direct_call($this->factomd_url, "commit-entry", array("message" => $message));
    }

    // Wrapper function around a call to the reveal-entry method in the Factomd API
    public function revealEntry($entry)
    {
        return $this->direct_call($this->factomd_url, "reveal-entry", array("entry" => $entry));
    }

    // High level function that commits and reveals a entry
    public function commitRevealEntry($chainId, $extIds, $content)
    {
        $composed = $this->composeEntry($chainId, $extIds, $content);

        if (gettype($composed) != "array" || !array_key_exists("result", $composed))
            return $composed;

        $message = $composed['result']['commit']['params']['message'];
        $entry = $composed['result']['reveal']['params']['entry'];

        $commit = $this->commitEntry($message);
        $reveal = $this->revealEntry($entry);
        return array("commit" => $commit, "reveal" => $reveal);
    }

    // Wrapper function around a call to the commit-chain method in the Factomd API
    public function commitChain($message)
    {
        return $this->direct_call($this->factomd_url, "commit-chain", array("message" => $message));
    }

    // Wrapper function around a call to the reveal-chain method in the Factomd API
    public function revealChain($entry)
    {
        return $this->direct_call($this->factomd_url, "reveal-chain", array("entry" => $entry));
    }

    // High level function that commits and reveals a chain
    public function commitRevealChain($extIds, $content)
    {
        $composed = $this->composeChain($extIds, $content);
        if (gettype($composed) != "array" || !array_key_exists("result", $composed))
            return $composed;

        $message = $composed['result']['commit']['params']['message'];
        $entry = $composed['result']['reveal']['params']['entry'];

        $commit = $this->commitChain($message);
        $reveal = $this->revealChain($entry);
        return array("commit" => $commit, "reveal" => $reveal);
    }

    public function getEntry ($entryHash) {
        $result = $this->direct_call($this->factomd_url, "entry", array("hash" => $entryHash));
        if ($this->prefer_ascii) {
            // NOTE: json decoding the result content is Factomize plugin specific, content doesn't have to be in the json format
            $dehexed_result = self::dehexify($result['result']['content']);
            $result['result']['content'] = json_decode($dehexed_result, 1);
            if ($result['result']['content'] == null)
                $result['result']['content'] = $dehexed_result;

            foreach($result['result']['extids'] as &$extId) {
                $extId = self::dehexify($extId);
            }
        return $result;
        }
    }

    public function getChainHead ($chain_id) {
        $result = $this->direct_call($this->factomd_url, "chain-head", array("chainid" => $chain_id));
        return $result;
    }

    public function getAck ($hash, $chain_id) {
        $result = $this->direct_call($this->factomd_url, "ack", array("hash" => $hash, "chainid" => $chain_id));
        return $result;
    }

    public function entryAcknowledgeStatus ($entry_hash, $chain_id) {
        $result = $this->getAck($entry_hash, $chain_id);
        if (gettype($result) == "array" && array_key_exists("result", $result)) {
            return $result['result']['entrydata']['status'];
        }
        else {
            return $result;
        }
    }

    public function entryConfirmed ($entry_hash, $chain_id) {
        $ack = $this->entryAcknowledgeStatus ($entry_hash, $chain_id);
        return $ack == "DBlockConfirmed";
    }

    public function chainIsProcessed ($chain_id) {
        $result = $this->getChainHead($chain_id);
        if (gettype($result) == "array" && array_key_exists("result", $result)) {
            return $result['result']['chainhead'] != "";
        }
        else {
            return $result;
        }
    }

    public function getHeights () {
        $result = $this->direct_call($this->factomd_url, "heights", array());
        return $result;
    }

    public function getDirectoryBlockHeight () {
        $result = $this->getHeights();
        if (gettype($result) == "array" && array_key_exists("result", $result)) {
            return $result['result']['directoryblockheight'];
        }
        else {
            return $result;
        }
    }

    public function getEntryBlock ($keymr) {
        $result = $this->direct_call($this->factomd_url, "entry-block", array("keymr" => $keymr));
        return $result;
    }

    public function getAllEntriesFollowingKeyMR ($keymr) {
        $entries = array();

        $prevKeyMR = $keymr;
        while ($prevKeyMR != self::ZEROHASH) {
            $entry_block = $this->getEntryBlock($prevKeyMR);
            if (gettype($entry_block) != "array" || !array_key_exists("result", $entry_block))
                return $entry_block;

            $reversed_entries = array_reverse($entry_block['result']['entrylist']);

            foreach ($reversed_entries as $entry) {
                $entries[] = $entry["entryhash"];
            }

            $prevKeyMR = $entry_block['result']['header']['prevkeymr'];
        }
        return $entries;
    }

    public function getAllEntryHashesInChain ($chain_id) {
        $chain_head_result = $this->getChainHead($chain_id);
        if (gettype($chain_head_result) != "array" || !array_key_exists("result", $chain_head_result))
            return $chain_head_result;

        $keymr = $chain_head_result['result']['chainhead'];

        return $this->getAllEntriesFollowingKeyMR($keymr);
    }

    public function getAllEntriesInChain ($chain_id) {
        $entry_hashes = $this->getAllEntryHashesInChain($chain_id);
        if (gettype($entry_hashes) != "array" || !array_key_exists(0, $entry_hashes))
            return $entry_hashes;

        $entries = array();
        foreach ($entry_hashes as $entry_hash) {
            $entry = $this->getEntry($entry_hash);
            if (!array_key_exists('result', $entry))
                return $entry;

            $entry_confirmed = $this->entryConfirmed($entry_hash, $chain_id);
            if ($entry_confirmed)
                $entries[] = $entry['result'];
        }

        return $entries;
    }

    public function getEntryCreditBalance ($ec=null) {
        if ($ec === null)
            $ec = $this->ec_address;
        $result = $this->direct_call($this->factomd_url, "entry-credit-balance", array("address" => $ec));
        return $result;
    }

    public function getAddress ($ec=null) {
        if ($ec === null)
            $ec = $this->ec_address;
        $result = $this->direct_call($this->factom_walletd_url, "address", array("address" => $ec));
        return $result;
    }
}
