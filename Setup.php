<?php

namespace CanonicalLedgers\Factomize;

use XF\AddOn\AbstractSetup;
use XF\AddOn\StepRunnerInstallTrait;
use XF\AddOn\StepRunnerUninstallTrait;
use XF\AddOn\StepRunnerUpgradeTrait;

use XF\Db\Schema\Alter;
use XF\Db\Schema\Create;

class Setup extends AbstractSetup
{
    use StepRunnerInstallTrait;
    use StepRunnerUpgradeTrait;
    use StepRunnerUninstallTrait;

    public function installStep1()
    {
        $this->schemaManager()->alterTable('xf_forum', function (Alter $table) {
            $table->addColumn('canonicalledgers_factomize_secure', 'bool')->setDefault(0);
            $table->addColumn('canonicalledgers_factomize_existing', 'bool')->setDefault(0);
        });

        $this->schemaManager()->alterTable('xf_thread', function (Alter $table) {
            $table->addColumn('canonicalledgers_factomize_chain_id', 'varchar(64)')->setDefault("0");
        });

        $this->schemaManager()->alterTable('xf_post', function (Alter $table) {
            $table->addColumn('canonicalledgers_factomize_entry_hash', 'varchar(500)')->setDefault("0");
            $table->addColumn('canonicalledgers_factomize_verification_status', 'varchar(20)')->setDefault("Unsecured");
            $table->addColumn('canonicalledgers_factomize_public_data', 'text');
        });
    }

    public function installStep2()
    {
        $this->schemaManager()->createTable('xf_canonicalledgers_factomize_security', function (Create $table) {
            $table->addColumn('id', 'int')->autoIncrement();
            $table->addColumn('explorer_template_link', 'varchar(255)')->setDefault("https://explorer.factom.com/chains/{chain_input}{/entries/entry_input}");
            $table->addColumn('last_block_height_checked', 'bigint')->setDefault(0);
            $table->addColumn('temp_public_key', 'text');
            $table->addColumn('current_public_key', 'text');
            $table->addPrimaryKey("id");
        });

        $cols = array(
            "last_block_height_checked" => "0",
            "temp_public_key" => "",
            "current_public_key" => ""
        );

        SQLHelpers::dbInsert('xf_canonicalledgers_factomize_security', $cols);
    }

    public function installStep3()
    {
        $this->schemaManager()->createTable('xf_canonicalledgers_factomize_queue', function (Create $table) {
            $table->addColumn('queue_id', 'int')->autoIncrement();
            $table->addColumn('public_data', 'text');
            $table->addColumn('extids', 'text');
            $table->addColumn('encoded_entry', 'text');
            $table->addColumn('chain_id', 'varchar(64)')->setDefault("0");
            $table->addColumn('entry_hash', 'varchar(64)')->setDefault("0");
            $table->addColumn('content_type', 'varchar(10)')->setDefault("post");
            $table->addColumn('last_step_performed_date', 'bigint');
            $table->addColumn('current_step', 'varchar(20)')->setDefault("commit");
            $table->addColumn('post_id', 'int')->setDefault(0);
            $table->addColumn('thread_id', 'int')->setDefault(0);
            $table->addColumn('last_block_height_checked', 'bigint')->setDefault(0);
            $table->addPrimaryKey("queue_id");
        });

        $db = \XF::db();
        $db->query("
            ALTER TABLE xf_canonicalledgers_factomize_queue
            MODIFY encoded_entry text null
        ");
        $db->query("
            ALTER TABLE xf_post
            MODIFY canonicalledgers_factomize_public_data text null
        ");
    }

    public function installStep4()
    {
        $this->schemaManager()->createTable('xf_canonicalledgers_factomize_info', function (Create $table) {
            $table->addColumn('thread_chain_protocol', 'varchar(6)');
            $table->addColumn('identity_chain_protocol', 'varchar(6)');
            $table->addColumn('addon_version', 'varchar(6)');
        });

        $cols = array(
            "thread_chain_protocol" => "v1.0.0",
            "identity_chain_protocol" => "v1.0.0",
            "addon_version" => "v1.0.0"
        );

        SQLHelpers::dbInsert('xf_canonicalledgers_factomize_info', $cols);
    }

    public function uninstallStep1()
    {
        $this->schemaManager()->alterTable('xf_forum', function(Alter $table)
        {
            $table->dropColumns('canonicalledgers_factomize_secure');
            $table->dropColumns('canonicalledgers_factomize_existing');
        });

        $this->schemaManager()->alterTable('xf_thread', function(Alter $table)
        {
            $table->dropColumns('canonicalledgers_factomize_chain_id');
        });

        $this->schemaManager()->alterTable('xf_post', function(Alter $table)
        {
            $table->dropColumns('canonicalledgers_factomize_entry_hash');
            $table->dropColumns('canonicalledgers_factomize_verification_status');
            $table->dropColumns('canonicalledgers_factomize_public_data');
        });
    }

    public function uninstallStep2 ()
    {
        $this->schemaManager()->dropTable("xf_canonicalledgers_factomize_security");
    }

    public function uninstallStep3 ()
    {
        $this->schemaManager()->dropTable("xf_canonicalledgers_factomize_queue");
    }

    public function uninstallStep4 ()
    {
        $this->schemaManager()->dropTable("xf_canonicalledgers_factomize_info");
    }

    private function deleteDirectory($dir) {
        if (!file_exists($dir))
            return true;

        if (!is_dir($dir))
            return unlink($dir);

        foreach (scandir($dir) as $item) {
            if ($item === '.' || $item === '..')
                continue;

            if (!self::deleteDirectory($dir . DIRECTORY_SEPARATOR . $item))
                return false;
        }
        
        return rmdir($dir);
    }

    public function uninstallStep5 ()
    {
        self::deleteDirectory('internal_data/CanonicalLedgers_Factomize');
    }
}
