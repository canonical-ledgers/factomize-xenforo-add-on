<?php

namespace CanonicalLedgers\Factomize\Cron;
use CanonicalLedgers\Factomize;
use CanonicalLedgers\Factomize\Cron\Error;
use CanonicalLedgers\Factomize\Handler;
use CanonicalLedgers\Factomize\Listener;
use CanonicalLedgers\Factomize\SecureKey;
use CanonicalLedgers\Factomize\SQLHelpers;

class FactomQueue
{
    // Sends a SQL statement to update a column in the factomize queue to the passed in value
    // and simultaneously updates the last_step_performed_date to now
    public static function updateQueueColumnWithTimeStamp ($queue_id, $col, $val) {
        $cols = array(
            $col => $val,
            "last_step_performed_date" => time()
        );

        $where = "queue_id = " . $queue_id;
        SQLHelpers::updateTable('xf_canonicalledgers_factomize_queue', $cols, $where);
    }

    // Updates the value of encoded entry in the queue table for the specified queue_id
    public static function updateEncodedEntry ($queue_id, $entry) {
        self::updateQueueColumnWithTimeStamp($queue_id, 'encoded_entry', $entry);
    }

    // Gets the value of encoded entry in the queue table for the specified queue_id
    public static function getEncodedEntry ($queue_id) {
        $where = "queue_id = " . $queue_id;
        return SQLHelpers::getTableColumn('xf_canonicalledgers_factomize_queue', 'encoded_entry', $where);
    }

    // Updates the current_step of a queue_id to a passed step
    public static function updateInQueueStep ($queue_id, $step) {
        self::updateQueueColumnWithTimeStamp($queue_id, 'current_step', $step);
    }

    // Updates the current_step of a queue_id to a passed step
    public static function restartQueueEntry ($queue_id) {
        self::updateQueueColumnWithTimeStamp($queue_id, 'current_step', "commit");
    }

    // Updates the chain_id of a queue_id to a passed chain_id
    public static function updateQueueChainID ($queue_id, $chain_id) {
        self::updateEncodedEntry($queue_id, "");
        self::updateEntryHash($queue_id, "");
        self::updateQueueColumnWithTimeStamp($queue_id, 'chain_id', $chain_id);
    }

    // Gets the chain_id for a specified queue_id
    public static function getQueueChainID ($queue_id) {
        $where = "queue_id = " . $queue_id;
        return SQLHelpers::getTableColumn('xf_canonicalledgers_factomize_queue', 'chain_id', $where);
    }

    // Updates the entry hash for a queue_id
    public static function updateEntryHash ($queue_id, $entry_hash) {
        self::updateQueueColumnWithTimeStamp($queue_id, 'entry_hash', $entry_hash);
    }

    // Gets the entry hash for a queue_id
    public static function getEntryHash ($queue_id) {
        $where = "queue_id = " . $queue_id;
        return SQLHelpers::getTableColumn('xf_canonicalledgers_factomize_queue', 'entry_hash', $where);
    }

    // Removes a row in the queue by queue_id
    public static function removeQueueRow ($queue_id) {
        SQLHelpers::deleteTableRow('xf_canonicalledgers_factomize_queue', 'queue_id = ' . $queue_id);
    }

    // Takes a passed entry hash, generally from the queue, and updates the entry hash column in xf_post with it
    public static function addEntryHashToPost ($post_id, $entry_hash) {
        $where = "post_id = " . $post_id;
        SQLHelpers::updateTable('xf_post', array('canonicalledgers_factomize_entry_hash' => $entry_hash), $where);
    }

    // Updates the external ids in the queue
    public static function updateExtIds ($queue_id, $extids) {
        if (gettype($extids) == "array")
            $extids = json_encode($extids);
        $where = "queue_id = " . $queue_id;
        SQLHelpers::updateTable('xf_canonicalledgers_factomize_queue', array('extids' => $extids), $where);
    }

    // Update public data in the queue
    public static function updatePublicData ($queue_id, $data) {
        if (gettype($data) == "array")
            $data = json_encode($data);
        $where = "queue_id = " . $queue_id;
        SQLHelpers::updateTable('xf_canonicalledgers_factomize_queue', array('public_data' => $data), $where);
    }

    // Uses a SQL statement to match all posts in the queue with a specified thread_id and update them with the passed chain_id
    public static function addChainIDToAllThreadPostsInQueue ($thread_id, $chain_id) {
        $where = "thread_id = " . $thread_id . " AND content_type = 'post'";
        SQLHelpers::updateTable('xf_canonicalledgers_factomize_queue', array('chain_id' => $chain_id), $where);
    }

    // Updates the chain_id in xf_thread to the passed value
    public static function addChainIDToThread ($thread_id, $chain_id) {
        $thread_exists = SQLHelpers::getTableColumn('xf_thread', 'thread_id', 'thread_id = ' . $thread_id);
        if ($thread_exists) {
            $thread_finder = \XF::finder('XF::Thread');
            $thread_finder->where('thread_id', $thread_id)->fetchOne()->fastUpdate('canonicalledgers_factomize_chain_id', $chain_id);
        }
    }

    public static function dateAndSortPublicData ($public_data) {
        $public_data = json_decode($public_data, 1);

        // If public_data is not an array, return an error - this should never occur
        // TODO: decide what to do in this case - since commit is the first step, can't restart queue
        if (gettype($public_data) !== "array")
           return Error\Error::internalQueueError();


        $time = time();
        $public_data['entry_date'] = $time;

        ksort($public_data);
        if (array_key_exists('post_data', $public_data))
            ksort($public_data['post_data']);
        else if (array_key_exists('thread_data', $public_data))
            ksort($public_data['thread_data']);

        return $public_data;
    }

    public static function addSignatureToExtIds ($extids, $public_data) {
        $extids = json_decode($extids, 1);

        // If extids is not an array, return an error - this should never occur
        if (gettype($extids) !== "array")
           return Error\Error::internalQueueError();

        $signature = SecureKey::signFactomPost($public_data);
        if (!$signature)
            return Error\Error::publicKeyOutOfDate(null, "FactomQueue.php::FactomQueue::handleCommit::signFactomPost");

        array_splice($extids, 1, 0, $signature);

        return $extids;
    }

    public static function handleCompose ($queue_entry, $type) {
        $public_data = self::dateAndSortPublicData($queue_entry['public_data']);
        if (array_key_exists("error_occurred", $public_data))
            return $public_data;

        $extids = self::addSignatureToExtIds($queue_entry['extids'], $public_data);
        if (array_key_exists("error_occurred", $extids))
            return $extids;

        $factom = Handler::getFactomAPI();
        if ($type == "thread")
            $composed_commit = $factom->composeChain($extids, $public_data);
        else if ($type == "post")
            $composed_commit = $factom->composeEntry($queue_entry['chain_id'], $extids, $public_data);

        if (!array_key_exists("result", $composed_commit)) {
            return Error\Error::factomError($composed_commit, "FactomQueue.php::FactomQueue::handleCommit::FactomAPI-compose");
        }

        self::updatePublicData($queue_entry['queue_id'], json_encode($public_data, JSON_PRETTY_PRINT));

        $message = $composed_commit['result']['commit']['params']['message'];
        $entry = $composed_commit['result']['reveal']['params']['entry'];

        return array("message" => $message, "entry" => $entry);
    }

    // Generic function that can handle both a thread and post commit
    // Also, in this function, entry_date is added and post/thread data is sorted
    public static function handleCommit ($queue_entry, $type) {
        $factom = Handler::getFactomAPI();
        $composed = self::handleCompose($queue_entry, $type);
        if (array_key_exists("error_occurred", $composed))
            return $composed;

        $message = $composed['message'];
        $entry = $composed['entry'];

        if ($type == "thread")
            $commit = $factom->commitChain($message);
        else if ($type == "post")
            $commit = $factom->commitEntry($message);

        if (array_key_exists("error", $commit)) {
            return Error\Error::factomError($commit, "FactomQueue.php::FactomQueue::handleCommit::FactomAPI-commit");
        }

        self::updateEncodedEntry($queue_entry['queue_id'], $entry);
        self::updateInQueueStep($queue_entry['queue_id'], "reveal");

        return Error\Error::noError();
    }

    // If the queue entry already has a post added to it then just commit using this
    // If it doesn't, then check the xf_thread table and get the chain id from there
    // If neither of these cases are met, then the thread commit must still be processing
    // in the queue and is blocking a post from moving further.
    public static function handleCommitEntry ($post) {
        if (strlen($post['chain_id']) == 64)
            return self::handleCommit($post, "post");

        $thread_chain_id = Handler::getChainID($post['thread_id']);
        if ($thread_chain_id != "0") {
            self::updateQueueChainID($post['queue_id'], $thread_chain_id);
            $post['chain_id'] = $thread_chain_id;

            return self::handleCommit($post, "post");
        }
        else {
            return Error\Error::noError();
        }
    }

    // Wrapper around a call to commit a chain for a thread
    public static function handleCommitChain ($thread) {
        return self::handleCommit($thread, "thread");
    }

    // Generic function that can handle revealing threads/chains and posts/entries
    public static function handleReveal ($queue_entry, $type) {
        $factom = Handler::getFactomAPI();
        $entry = self::getEncodedEntry($queue_entry['queue_id']);

        if ($type == "thread")
            $reveal = $factom->revealChain($entry);
        else if ($type == "post")
            $reveal = $factom->revealEntry($entry);

        if (array_key_exists("error", $reveal)) {
            return $reveal;
        }

        $chain_id = $reveal['result']['chainid'];
        $entry_hash = $reveal['result']['entryhash'];
        self::updateQueueChainID($queue_entry['queue_id'], $chain_id);
        self::updateEntryHash($queue_entry['queue_id'], $entry_hash);
        self::updateInQueueStep($queue_entry['queue_id'], "dblockconfirm");

        return Error\Error::noError();
    }


    // Wrapper around revealing a chain for a thread
    public static function handleRevealChain ($thread) {
        return self::handleReveal($thread, "thread");
    }

    // Wrapper around revealing an entry for a post
    public static function handleRevealEntry ($post) {
        return self::handleReveal($post, "post");
    }

    public static function getPostEditCount ($post_id) {
        $where = "post_id = " . $post_id;
        return SQLHelpers::getTableColumn('xf_post', 'edit_count', $where);
    }

    // Generic function used to check whether a row in the queue (entry or chain)
    // has been successfully processed, if it has, then this function will "confirm" and
    // remove the row
    public static function confirmRow ($queue_entry, $type) {
        $factom = Handler::getFactomAPI();

        $entry_processed = $factom->entryConfirmed($queue_entry['entry_hash'], $queue_entry['chain_id']);

        if ($entry_processed === true) {
            if ($type == "thread") {
                $chain_id = $queue_entry['chain_id'];
                self::addChainIDToThread($queue_entry['thread_id'], $chain_id);
                self::addChainIDToAllThreadPostsInQueue($queue_entry['thread_id'], $chain_id);
            }
            else if ($type == "post") {
                $entry_hash = $queue_entry['entry_hash'];

                $public_data = json_decode($queue_entry['public_data'], 1);
                if (array_key_exists("post_data", $public_data)) {
                    $queue_edit_count = $public_data['post_data']['edit_count'];
                    $post_edit_count = self::getPostEditCount($queue_entry['post_id']);
                    if ($post_edit_count === $queue_edit_count)
                        self::addEntryHashToPost($queue_entry['post_id'], $entry_hash);
                }
                Handler::setPostStatus($queue_entry['post_id'], "Secured");
                Handler::setPublicData($queue_entry['post_id'], $queue_entry['public_data']);
            }
            self::removeQueueRow($queue_entry['queue_id']);
        }
        else if (gettype($entry_processed) == "array") {
            return Error\Error::factomError($entry_processed);
        }

        return Error\Error::noError();
    }

    // Wrapper around checking whether a thread/chain is done processing
    public static function confirmChain ($thread) {
        return self::confirmRow($thread, 'thread');
    }

    // Wrapper around checking whether a post/entry is done processing
    public static function confirmEntry ($post) {
        return self::confirmRow($post, 'post');
    }

    // State machine handler function for queue entries tagged as threads, this
    // function advances the state of threads in the queue until they are finally
    // confirmed and removed from the queue
    public static function advanceThread ($thread) {
        if ($thread['current_step'] == "commit") {
            $status = self::handleCommitChain($thread);
        }
        else if ($thread['current_step'] == "reveal") {
            $status = self::handleRevealChain($thread);
        }
        else if ($thread['current_step'] == "dblockconfirm") {
            $status = self::confirmChain($thread);
        }

        return $status;
    }

    // State machine handler function for queue entries tagged as posts, this
    // function advances the state of posts in the queue until they are finally
    // confirmed and removed from the queue
    public static function advancePost ($post) {
        if ($post['current_step'] == "commit") {
            $status = self::handleCommitEntry($post);
        }
        else if ($post['current_step'] == "reveal") {
            $status = self::handleRevealEntry($post);
        }
        else if ($post['current_step'] == "dblockconfirm") {
            $status = self::confirmEntry($post);
        }

        return $status;
    }

    // Wrapper around the call to the php Factom API wrapper class to get the
    // current directory block height
    public static function getDBlockHeight () {
        $factom = Handler::getFactomAPI();
        return $factom->getDirectoryBlockHeight();
    }

    // Updates the last checked queue height of a queue entry to the passed height
    public static function updateLastCheckedQueueHeight ($entry, $height) {
        $where = "queue_id = " . $entry['queue_id'];
        SQLHelpers::updateTable('xf_canonicalledgers_factomize_queue', array('last_block_height_checked' => $height), $where);
    }

    // Wrapper that returns the entire queue table
    public static function getFactomizeQueue () {
        $table = SQLHelpers::getTable('xf_canonicalledgers_factomize_queue');
        return $table;
    }

    // Options are validated when submitted so the only way they will fail is if they have not been specified
    public static function testAllOptions () {
        $options = \XF::options();
        if (!Listener::verifyIdentityChainAddress($options->identityChainAddress))
            return Error\Error::misconfiguredOptions("The identity chain option has not been specified.");
        else if (!Listener::verifyEntryCreditAddress($options->entryCreditAddress))
            return Error\Error::misconfiguredOptions("The entry credit address option has not been specified.");
        else if (!Listener::verifyEndpointAddress($options->factomdEndpointAddress))
            return Error\Error::misconfiguredOptions("The factomd endpoint address option has not been specified.");
        else if (!Listener::verifyEndpointAddress($options->factomWalletdEndpointAddress))
            return Error\Error::misconfiguredOptions("The factom-walletd endpoint address option has not been specified.");

        return true;
    }

    // Cron-entry entry point. From this function, the entry queue/state machine is
    // advanced through the process of commiting, revealing, and confirming entries
    // to the blockchain
    public static function advanceQueue () {
        $testOptions = self::testAllOptions();
        if ($testOptions !== true)
            throw new \Exception(json_encode($testOptions));

        // Verify that we have a valid signing key that matches id chain
        $current_height = self::getDBlockHeight();
        $id_status = SecureKey::pullIdentityChain($current_height);

        if ($id_status['error_occurred'])
            throw new \Exception(json_encode($id_status));

        // Start over if any step takes longer than 2 hours
        $max_time_for_step = 2 * 60 * 60;

        $table = self::getFactomizeQueue();
        foreach ($table as $entry) {
            $last_checked_height = $entry['last_block_height_checked'];
            if ($last_checked_height == $current_height && $entry['current_step'] !== "reveal")
                continue;

            if ($entry["content_type"] == "thread") {
                $status = self::advanceThread($entry);
            }
            else if ($entry["content_type"] == "post") {
                $status = self::advancePost($entry);
            }

            if ($status["error_occurred"])
                throw new \Exception(json_encode($status));

            $secs_since_last_step = time() - $entry['last_step_performed_date'];
            if ($secs_since_last_step > $max_time_for_step) {
                self::updateInQueueStep($entry['queue_id'], "commit");
            }
            self::updateLastCheckedQueueHeight($entry, $current_height);
        }
    }
}
