<?php

namespace CanonicalLedgers\Factomize\Cron\Error;

class Error {
    public static function formError ($error_type="", $error_message="", $error_trail_marker, $error_occured=true) {
        return array (
            "error_occurred" => $error_occured,
            "type" => $error_type,
            "message" => $error_message,
            "error_trail" => array($error_trail_marker)
        );
    }

    public static function noError () {
        return self::formError(
            "SUCCESS",
            "NO ERROR OCCURRED",
            "",
            false
        );
    }

    public static function misconfiguredOptions ($error_message=null, $error_trail_marker="") {
        $error_message = !$error_message ? "The add-on options were not configured properly, please retry." : $error_message;
        return self::formError(
            "MISCONFIGURED OPTIONS",
            $error_message,
            $error_trail_marker
        );
    }


    public static function invalidIdentityChain ($error_message=null, $error_trail_marker="") {
        $error_message = !$error_message ? "The identity chain has not been setup and configured properly." : $error_message;
        return self::formError(
            "NO VALID IDENTITY CHAIN",
            $error_message,
            $error_trail_marker
        );
    }

    public static function publicKeyOutOfDate ($error_message=null, $error_trail_marker="") {
        $error_message = !$error_message
        ? "The configured identity chain has a more recent valid key than the server. Follow steps to regenerate a private key to fix this error."
        : $error_message;

        return self::formError(
            "PUBLIC KEY OUT OF DATE",
            $error_message,
            $error_trail_marker
        );
    }

    public static function failedToConnectToFactom ($error_message=null, $error_trail_marker="") {
        $error_message = !$error_message ? "A connection to factom could not be established with the endpoints provided." : $error_message;
        return self::formError(
            "FAILED TO CONNECT TO FACTOM",
            $error_message,
            $error_trail_marker
        );
    }

    public static function factomError ($error_message=null, $error_trail_marker="") {
        $error_message = !$error_message ? "An error occurred while making a request to Factom." : $error_message;
        return self::formError(
            "FACTOM ERROR",
            $error_message,
            $error_trail_marker
        );
    }

    public static function internalQueueError ($error_message=null, $error_trail_marker="") {
        $error_message = !$error_message ? "The queue encountered improperly added data!" : $error_message;
        return self::formError(
            "INTERNAL QUEUE ERROR",
            $error_message,
            $error_trail_marker
        );
    }
}
